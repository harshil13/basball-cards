from ast import Try
from cgi import test
from email.message import Message
from email.mime import image
from logging import exception
import os
from urllib import response
import boto3 
import urllib
import json



def detect_labels(bucket,key):
    rekognition= boto3.client('rekognition',region_name='eu-west-1')
    response = rekognition.detect_text(Image={'S3Object':{'Bucket':bucket,'Name':key}})
    textdetections=response['TextDetections']
    print ('detected text')
    for text in textdetections:
        print('detected text:'+ text['DetectedText'])
        print ('Confidence: ' + "{:.2f}".format(text['Confidence']) + "%")
        print
    return response


def publish_email():
    topic_arn="arn:aws:sns:eu-west-1:296274010522:sns_topic_name"
    sns=boto3.client("sns")
    response=sns.publish(TopicArn='arn:aws:sns:eu-west-1:296274010522:sns_topic_name',Message="file uploaded")



def lambda_handler(event,context):
    bucket=event['Records'][0]['s3']['bucket']['name']
    key=urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'],encoding='utf-8')

    response=detect_labels(bucket,key)
    publish_email()
    
    
    return "{} from lambda".format(os.environ['foo'])