resource "aws_sns_topic" "sns_topic_test" {
    name = "sns_topic_name"
}

resource "aws_sns_topic_subscription" "sns_topic_subcription" {
    topic_arn = aws_sns_topic.sns_topic_test.arn
    protocol = "email"
    endpoint = "harshilsheganlall1304@gmail.com"
  
}