provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}


resource "aws_s3_bucket" "harshil-test-s3" {
  bucket = "harshil-test"
  acl    = "private"
  tags = {
    Name        = "Harshil-s3-test"
    environment = "Dev"
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.harshil-test-s3.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.harshil-test-s3.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [
    aws_lambda_permission.allow_bucket
  ]

}


