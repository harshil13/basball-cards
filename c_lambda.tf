

resource "aws_iam_role" "iam-for-lambda" {
  name               = "iam-for-lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",

      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}



resource "aws_iam_policy" "lambda_loggings" {
  name        = "lambda_loggings"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:*"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "rekognition_access" {
  name = "rekognition_access"
  path = "/"
  description = "policy for rekognition access for lambda"

  policy= jsonencode(
  {
    "Version": "2012-10-17",
    "Statement": [
      {
      "Action":"rekognition:*",
      "Effect":"Allow",
      "Resource":"*"
      }
    ]
  })
  
}

resource "aws_iam_policy" "S3_access" {
  name = "S3_access"
  path = "/"
  description = "policy for s3 access for lambda/rekog"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action":"s3:*",
        "Effect":"Allow",
        "Resource":"*"
      }
    ]
  }
  )
}

resource "aws_iam_policy" "SNS_access" {
  name="SNS_access"
  path="/"
  description = "policy for lambda to publish on sns"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action":"sns:*",
        "Effect":"Allow",
        "Resource":"*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "sns_attach" {
  role=aws_iam_role.iam-for-lambda.name
  policy_arn = aws_iam_policy.SNS_access.arn
}

resource "aws_iam_role_policy_attachment" "S3_policyatt" {
  role = aws_iam_role.iam-for-lambda.name
  policy_arn = aws_iam_policy.S3_access.arn
}
resource "aws_iam_role_policy_attachment" "rekognition_access" {
  role = aws_iam_role.iam-for-lambda.name
  policy_arn = aws_iam_policy.rekognition_access.arn
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam-for-lambda.name
  policy_arn = aws_iam_policy.lambda_loggings.arn
}


resource "aws_lambda_function" "test_lambda" {
  filename      = "hello_lambda.zip"
  function_name = "hello_lambda"
  role          = aws_iam_role.iam-for-lambda.arn
  handler       = "hello_lambda.lambda_handler"

  runtime = "python3.6"

  depends_on = [
    aws_iam_role_policy_attachment.lambda_logs,
  ]

  environment {
    variables = {
      foo = "bar"
    }
  }
}


